package com.example.myfirstapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myfirstapp.db.entities.Meter
import com.example.myfirstapp.db.entities.User

@Database(
    entities = [User::class, Meter::class],
    version = 3
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun user(): UserDao
    abstract fun meter(): MeterDao

    companion object {
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if(instance == null){
                instance = Room
                    .databaseBuilder(context.applicationContext, AppDatabase::class.java, "my_db")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance as AppDatabase
        }
    }
}