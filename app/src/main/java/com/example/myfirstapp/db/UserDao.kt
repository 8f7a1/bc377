package com.example.myfirstapp.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.myfirstapp.db.entities.User

@Dao
interface UserDao {
    @Query("SELECT * FROM User")
    fun getUsers() : List<User>

    @Insert
    suspend fun createUser(vararg u: User)

    @Delete
    suspend fun deleteUser(u: User)

    @Query("DELETE FROM User WHERE id=:userId")
    suspend fun deleteUserById(userId: Int)
}