package com.example.myfirstapp.db

import androidx.room.*
import com.example.myfirstapp.db.entities.Meter

@Dao
interface MeterDao {

    @Query("SELECT * FROM Meter")
    fun getMeters(): List<Meter>

    @Insert
    suspend fun createMeter(vararg meter: Meter)

    @Delete
    suspend fun removeMeter(meter: Meter)

    @Update
    suspend fun updateMeter(meter: Meter)

    @Query("SELECT * FROM Meter WHERE id = :meterId")
    suspend fun getMeter(meterId: Long): Meter
}