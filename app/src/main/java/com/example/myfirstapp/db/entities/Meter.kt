package com.example.myfirstapp.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Meter(val meterName: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    var activePowImp: Float? = null
    var activePowExp: Float? = null
    var voltage: Float? = null
}
