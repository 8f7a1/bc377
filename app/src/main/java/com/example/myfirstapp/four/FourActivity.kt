package com.example.myfirstapp.four

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.viewModels
import com.example.myfirstapp.R

class FourActivity : AppCompatActivity() {

    private val vm: FourViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_four)

        supportActionBar?.apply {
            title = "Four Activity"
            setDisplayHomeAsUpEnabled(true)
        }

        val editText = findViewById<EditText>(R.id.edit_text_name)
        val submitBtn = findViewById<Button>(R.id.submit)
        val textView = findViewById<TextView>(R.id.textView)
        val returnBtn = findViewById<Button>(R.id.return_to_main)

        textView.text = vm.txt

        submitBtn.setOnClickListener {
            vm.txt = editText.text.toString()
            textView.text = vm.txt
        }

        returnBtn.setOnClickListener {
            val i = Intent()
            i.putExtra("txt", vm.txt)
            setResult(RESULT_OK, i)
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}