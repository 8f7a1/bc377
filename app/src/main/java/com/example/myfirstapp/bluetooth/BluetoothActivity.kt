package com.example.myfirstapp.bluetooth

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.pm.PackageManager
import android.icu.text.DecimalFormat
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelUuid
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.example.myfirstapp.R
import com.example.myfirstapp.db.AppDatabase
import com.example.myfirstapp.db.entities.Meter
import gurux.common.GXCommon
import gurux.dlms.*
import gurux.dlms.enums.Authentication
import gurux.dlms.enums.InterfaceType
import gurux.dlms.enums.ObjectType
import kotlinx.coroutines.*
import java.util.*

class BluetoothActivity : AppCompatActivity() {
    private lateinit var requestBluetoothConnect: ActivityResultLauncher<String>
    private lateinit var requestLocation: ActivityResultLauncher<String>
    private lateinit var requestBluetoothScan: ActivityResultLauncher<String>

    lateinit var meterName: String
    var meterId: Long = 0
    lateinit var myMeter: Meter

    lateinit var activePowerImpView: TextView
    lateinit var activePowerExpView: TextView
    lateinit var voltageView: TextView

    lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth)

        intent.getStringExtra("meterName")?.let {
            meterName = it
            supportActionBar?.title = meterName
        }

        meterId = intent.getLongExtra("meterId", 0)

        db = AppDatabase.getDatabase(this)

        runBlocking {
            launch(Dispatchers.IO) {
                myMeter = db.meter().getMeter(meterId)
            }
        }

        activePowerImpView = findViewById(R.id.active_power_imp)
        activePowerExpView = findViewById(R.id.active_power_exp)
        voltageView = findViewById(R.id.voltage)

        activePowerImpView.text = "Active Power Import    ${myMeter.activePowImp}  kWh"
        activePowerExpView.text = "Active Power Export    ${myMeter.activePowExp}  kWh"
        voltageView.text = "Active Power Import    ${myMeter.voltage}  V"


        val scanButton = findViewById<Button>(R.id.read_button)
        scanButton.setOnClickListener {
            scanBluetooth()
        }

        val saveButton = findViewById<Button>(R.id.save_button)
        saveButton.setOnClickListener {
            runBlocking {
                launch(Dispatchers.IO) {
                    db.meter().updateMeter(myMeter)
                }
            }
            finish()
        }

        requestBluetoothConnect =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                if (it) {
                    Log.d("permission", "user granted")
                    scanBluetooth()
                } else {
                    Log.d("permission", "user denied")
                }
            }

        requestBluetoothScan =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                if (it) {
                    scanBluetooth()
                }
            }

        requestLocation = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                scanBluetooth()
            }
        }
    }

    private fun displayValue(name: String, value: String, unit: String = "") {
        if(name == "activePowImp"){
            activePowerImpView.text = "Active Power Import    $value  $unit"
            myMeter.activePowImp = value.toFloatOrNull()
        }
        if(name == "activePowExp"){
            activePowerExpView.text = "Active Power Export    $value  $unit"
            myMeter.activePowExp = value.toFloatOrNull()
        }
        if(name == "voltage"){
            voltageView.text = "Voltage                $value  $unit"
            myMeter.voltage = value.toFloatOrNull()
        }
    }

    private fun scanBluetooth() {

        val bluetoothManager = getSystemService(BluetoothManager::class.java)
        val bluetoothAdapter = bluetoothManager.adapter

        // query paired devices
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestLocation.launch(Manifest.permission.ACCESS_FINE_LOCATION)
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.BLUETOOTH_SCAN
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestBluetoothScan.launch(Manifest.permission.BLUETOOTH_SCAN)
                return
            }

            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.BLUETOOTH_CONNECT
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestBluetoothConnect.launch(Manifest.permission.BLUETOOTH_CONNECT)
                return
            }
        }
        val pairedDevices = bluetoothAdapter.bondedDevices
        Log.d("permission", "system granted")
        pairedDevices.forEach {
            Log.d("device", it.name)
        }

        val builder: ScanFilter.Builder = ScanFilter.Builder()
            .setServiceUuid(ParcelUuid.fromString("b973f2e0-b19e-11e2-9e96-0800200c9a66"))

        val scanFilter: ScanFilter by lazy { builder.build() }
        val scanList: List<ScanFilter> by lazy { listOf(scanFilter) }
        val scanSetting: ScanSettings by lazy { ScanSettings.Builder().build() }

        val autoLeScanCallback: ScanCallback = object : ScanCallback() {
            @SuppressLint("MissingPermission")
            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                super.onScanResult(callbackType, result)
                if (result?.device?.name == meterName) {
                    Log.d("scanBLE", result.device?.address.toString())
                    bluetoothAdapter.bluetoothLeScanner.stopScan(this)
                    connectDevice(result.device!!)
                }
            }
        }

        bluetoothAdapter.bluetoothLeScanner.startScan(scanList, scanSetting, autoLeScanCallback)

    }

    var bluetoothGatt: BluetoothGatt? = null

    @SuppressLint("MissingPermission")
    fun connectDevice(device: BluetoothDevice) {
        bluetoothGatt = device.connectGatt(this, true, bluetoothGattCallback)
        waitUntilInitialized()
    }

    var writeCharacteristic: BluetoothGattCharacteristic? = null
    var readCharacteristic: BluetoothGattCharacteristic? = null

    val bluetoothGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {

        @SuppressLint("MissingPermission")
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt.discoverServices()
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                gatt.disconnect()
            }
        }

        @SuppressLint("MissingPermission")
        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            println("onServicesDiscovered work: ")
            val gattService: BluetoothGattService =
                gatt.getService(UUID.fromString("b973f2e0-b19e-11e2-9e96-0800200c9a66"))

            val writeCharacteristic =
                gattService.getCharacteristic(UUID.fromString("e973f2e2-b19e-11e2-9e96-0800200c9a66"))
            bluetoothGatt?.writeCharacteristic(writeCharacteristic)

            val readCharacteristic =
                gattService.getCharacteristic(UUID.fromString("d973f2e1-b19e-11e2-9e96-0800200c9a66"))
            bluetoothGatt?.setCharacteristicNotification(readCharacteristic, true)

            readCharacteristic!!.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
            val descriptor: BluetoothGattDescriptor =
                readCharacteristic!!.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"))
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            bluetoothGatt?.writeDescriptor(descriptor)
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt,
            descriptor: BluetoothGattDescriptor,
            status: Int
        ) {
            super.onDescriptorWrite(gatt, descriptor, status)
            writeCharacteristic = descriptor.getCharacteristic().getService()
                .getCharacteristic(UUID.fromString("e973f2e2-b19e-11e2-9e96-0800200c9a66"));
            readCharacteristic = descriptor.getCharacteristic().getService()
                .getCharacteristic(UUID.fromString("d973f2e1-b19e-11e2-9e96-0800200c9a66"));
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            var hex = GXCommon.bytesToHex(characteristic.value)
            println("ReceiveData: ${hex}")
            replyData = characteristic.value
        }
    }

    fun waitUntilInitialized() {
        runBlocking {
            val result = withTimeoutOrNull(7000L) {
                while (writeCharacteristic == null) {
                    delay(1000)
                }
                "writeCharacteristic initialized successfully"
            }
            if (result == null) {
                println("Connect Gatt Timeout.")
                waitUntilInitialized()
            } else {
                println(result)
                connectDLMS()
//                dlmsService.connectDLMS(meter)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun disconnect() {
        if (writeCharacteristic != null) {
            writeCharacteristic = null
            readCharacteristic = null
        }
        bluetoothGatt!!.disconnect()
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    val pass: String = "00000001"
    val client by lazy { GXDLMSClient(true, 32, 1, Authentication.LOW, pass, InterfaceType.HDLC) }

    var sendStatus: Boolean? = null

    var replyData: ByteArray? = null

    fun waitReply() {
        runBlocking {
            launch {
                while (replyData == null) {
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun sendData(data: ByteArray): Boolean {
        try {
            var hex = GXCommon.bytesToHex(data)
            println("SendData: ${hex}")
            writeCharacteristic?.value = data
            bluetoothGatt?.writeCharacteristic(writeCharacteristic)
            return true
        } catch (e: Exception) {
            println("SendDataError: ")
            println(e)
            println(e.message)
            return false
        }
    }

    fun readDLMSPacket(data: ByteArray, reply: GXReplyData? = null) {
        var count = 0
        sendStatus = null
        replyData = null
        while (count < 3) {
            sendStatus = sendData(data)
            if (sendStatus == true) {
//                waitUntilInitialized()
                waitReply()
                if (replyData != null) {
                    count = 10
                    if (replyData != null) {
                        val byteBuffer = GXByteBuffer()
                        byteBuffer.data = replyData
                        reply?.data = byteBuffer
                    }
                } else {
                    count++
                    println("Timeout readDLMS: $count")
                    println("DLMS read data Timeout.")
                    sendStatus = false
                }
            } else {
                count++
            }
        }
    }

    private fun getData(
        OBIS: String,
        name: String,
        unit: String,
        type: ObjectType,
        scale: Double
    ) {
        try {
            val getDatas = client.read(OBIS, type, 2)
            for (getData in getDatas) {
                setPacketInfo(getData, name, unit, scale)
            }
        } catch (e: Exception) {
            println("GetData Error : ")
            println(e.message)
        }
    }

    fun setPacketInfo(data: ByteArray?, name: String, unit: String, scale: Double) {
        replyData = null
        var count = 0
        while (count < 3) {
            sendData(data!!)
            if (sendStatus == true) {
                waitReply()
                if (replyData != null) {
                    count = 3
                    readData(name, unit, scale)
                } else {
                    count++
                    println("Timeout readDLMSData: $count")
                }
            }
        }
    }

    var translateXML = GXDLMSTranslator(TranslatorOutputType.SIMPLE_XML)

    private fun readData(name: String, unit: String, scale: Double) {
        try {
            val byteBuffer = GXByteBuffer()
            byteBuffer.set(replyData)
            byteBuffer.position(15)
            var value = GXDLMSClient.getValue(byteBuffer)
            value = value.toString().toInt()
            val decimalFormat = DecimalFormat("0.###")
            value = decimalFormat.format(value * scale).toString()
//            val info = Info(name, value, unit)
//            dataViewModel.setUpdateInfo(info)
            println("$name: $value")
//            var textView = findViewById<TextView>(R.id.textView2)
//            textView.text = value

            displayValue(name, value, unit)
            var xml = translateXML.messageToXml(replyData)
            println(xml)

        } catch (e: Exception) {
            println("Error")
            println(e.message)
        }
    }

    fun connectDLMS() {
        val reply = GXReplyData()
        val data = client.snrmRequest(true)
        if (data != null) {
            readDLMSPacket(data, reply)
            client.parseUAResponse(reply.data)
        }

        if (replyData != null) {
            for (it in client.aarqRequest()) {
                reply.clear()
                readDLMSPacket(it, reply)
            }
            println("Done Authentication: ")
            if (sendStatus == true) {
                getData("1.0.1.8.0.255", "activePowImp", "kWh", ObjectType.REGISTER, 0.001)
                getData("1.0.2.8.0.255", "activePowExp", "kWh", ObjectType.REGISTER, 0.001)
                getData("1.0.12.3.0.255", "voltage", "V", ObjectType.REGISTER, 0.01)
            }
            dlmsDisconnect()
            disconnect()
        }
    }

    fun dlmsDisconnect() {
        val data = client.disconnectRequest(true)
        if (data != null) {
            println("Start Disconnect")
            readDLMSPacket(data)
            if (replyData != null) {
                println("Disconnect: ")
            }
        }
    }


}