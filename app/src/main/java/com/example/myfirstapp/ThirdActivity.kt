package com.example.myfirstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class ThirdActivity : AppCompatActivity() {

    var txt: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        supportActionBar?.apply {
            title = "Third Activity"
            setDisplayHomeAsUpEnabled(true)
        }

        val editText = findViewById<EditText>(R.id.edit_text_name)
        val submitBtn = findViewById<Button>(R.id.submit)
        val textView = findViewById<TextView>(R.id.textView)

        if (savedInstanceState == null) {
            textView.text = txt
        } else {
            Log.d("create", "new")
        }

        submitBtn.setOnClickListener {
            txt = editText.text.toString()
            textView.text = "Hello $txt"
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("myName", txt)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        txt = savedInstanceState.getString("myName")
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}