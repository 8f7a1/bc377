package com.example.myfirstapp.meter_list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.myfirstapp.R

class MeterFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meter_form)

        val meterName = findViewById<EditText>(R.id.meter_name)
        val submitButton = findViewById<Button>(R.id.submit)

        submitButton.setOnClickListener {
            val resultIntent = Intent()
            resultIntent.putExtra("meterName", meterName.text.toString())
            setResult(RESULT_OK, resultIntent)
            finish()
        }
    }
}