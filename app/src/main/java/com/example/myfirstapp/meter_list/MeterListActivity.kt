package com.example.myfirstapp.meter_list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.example.myfirstapp.R
import com.example.myfirstapp.bluetooth.BluetoothActivity
import com.example.myfirstapp.db.AppDatabase
import com.example.myfirstapp.db.entities.Meter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MeterListActivity : AppCompatActivity() {
    var myMetersDb = listOf<Meter>()
    var myMeters = listOf<String>()
    lateinit var listView: ListView
    lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meter_list)

        supportActionBar?.apply {
            title = "รายการมิเตอร์"
        }

        db = AppDatabase.getDatabase(this)
        listView = findViewById<ListView>(R.id.meter_list_view)
        listView.setOnItemClickListener { adapterView, view, position, l ->
            // Toast.makeText(this, myMeters[position], Toast.LENGTH_LONG).show()
            val i = Intent(this, BluetoothActivity::class.java)
            i.putExtra("meterId", myMetersDb[position].id)
            i.putExtra("meterName", myMetersDb[position].meterName)
            startActivity(i)
        }

        listView.setOnCreateContextMenuListener { contextMenu, view, contextMenuInfo ->
            val menuInfo = (contextMenuInfo as AdapterContextMenuInfo)

            contextMenu.setHeaderTitle("Select action")
            contextMenu.add(0, 0, 0, "Remove")
                .setOnMenuItemClickListener {
                    val meterToRemove = myMetersDb[menuInfo.position]
                    runBlocking {
                        launch(Dispatchers.IO) {
                            db.meter().removeMeter(meterToRemove)
                        }
                    }
                    renderListView()
                    true
                }
        }

        val formLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if(it.resultCode == RESULT_OK){
                runBlocking {
                    launch(Dispatchers.IO) {
                        val meter = Meter(it.data!!.getStringExtra("meterName")!!)
                        db.meter().createMeter(meter)
                    }
                }
                renderListView()
            }
        }

        val addButton = findViewById<FloatingActionButton>(R.id.add_button)
        addButton.setOnClickListener {
            val i = Intent(this, MeterFormActivity::class.java)
            formLauncher.launch(i)
        }

        renderListView()
    }

    fun renderListView(){
        runBlocking {
            launch(Dispatchers.IO) {
                myMetersDb = db.meter().getMeters()
                myMeters = myMetersDb.map {
                    it.meterName
                }
            }
        }
        val adapter = ArrayAdapter<String>(this, R.layout.meter_item, R.id.meter_name, myMeters)
        listView.adapter = adapter
    }
}