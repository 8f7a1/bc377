package com.example.myfirstapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.example.myfirstapp.bluetooth.BluetoothActivity
import com.example.myfirstapp.db.AppDatabase
import com.example.myfirstapp.db.entities.User
import com.example.myfirstapp.four.FourActivity
import com.example.myfirstapp.meter_list.MeterListActivity
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    lateinit var fourActivityLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db = AppDatabase.getDatabase(this)


        // variable
        var x = 10

        // constant variable
        val y = 10

        // variable define type
        var z: Int

        // variable define type and nullable
        var s: String? = null

        // binding view to variable
        val gotoSecondaryBtn = findViewById<Button>(R.id.goto_secondary)
        val gotoThirdBtn = findViewById<Button>(R.id.goto_third)
        val gotoFourBtn = findViewById<Button>(R.id.goto_four)
        val gotoBluetoothBtn = findViewById<Button>(R.id.bluetooth_activity)
        val gotoMeterListBtn = findViewById<Button>(R.id.goto_meter_list)

        // set on click handler
        gotoSecondaryBtn.setOnClickListener { gotoSecondaryActivity() }
        gotoThirdBtn.setOnClickListener { gotoThirdActivity() }
        gotoFourBtn.setOnClickListener { gotoFourActivity() }
        gotoBluetoothBtn.setOnClickListener { gotoBluetoothActivity() }

        gotoMeterListBtn.setOnClickListener { gotoMeterListActivity() }

        fourActivityLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { r ->
                if (r.resultCode == RESULT_OK) {
                    r.data!!.getStringExtra("txt")?.let {
                        Log.d("txt", it)
                    }
                }
            }
    }

    private fun gotoMeterListActivity() {
        val i = Intent(this, MeterListActivity::class.java)
        startActivity(i)
    }

    private fun gotoSecondaryActivity() {
        Log.d("button", "goto to secondary clicked")

        // start activity
        val i = Intent(this, SecondaryActivity::class.java)
        startActivity(i)
    }

    private fun gotoThirdActivity() {
        // start activity
        val i = Intent(this, ThirdActivity::class.java)
        startActivity(i)
    }

    private fun gotoFourActivity() {
        // start activity
        val i = Intent(this, FourActivity::class.java)
        fourActivityLauncher.launch(i)
    }

    private fun gotoBluetoothActivity() {
        val i = Intent(this, BluetoothActivity::class.java)
        startActivity(i)
    }
}